//variables and const
const email = document.getElementById( 'email' );
const asunto = document.getElementById( 'asunto' );
const mensaje = document.getElementById( 'mensaje' );
const btnEnviar = document.getElementById( 'enviar' ); 
const btnReset = document.getElementById( 'resetBtn' );
//Event Listener
eventListeners();
function eventListeners() {
    //Inicio de la aplicacion y deshabilitar el submit
    document.addEventListener( 'DOMContentLoaded', inicioApp);

    //campos del formulario
    email.addEventListener('blur', validarCampo);
    asunto.addEventListener('blur', validarCampo);
    mensaje.addEventListener('blur', validarCampo);

    //Boton enviar en Submit
    btnEnviar.addEventListener('click', enviarEmail);
    //Boton reset
    btnReset.addEventListener( 'click', resetForm);
}
//Functions
function inicioApp() {
    // Deshabilitar envio
    btnEnviar.disabled = true;
}

//Cuando se envia el correo
function enviarEmail(e) {
    //Mostrar Spiner al Presionan Enviar
    const spinnerGIF = document.querySelector( '#spinner' );
    spinnerGIF.style.display = 'block';

    //Gif enviado
    const enviadoGIF = document.createElement( 'img' );
    enviadoGIF.src = 'img/mail.gif';
    enviadoGIF.style.display = 'block';

    //Ocultar SpinnerGIF y mostrar enviadoGIF
    setTimeout(() => {
        spinnerGIF.style.display = 'none'; 
        document.querySelector( '#loaders' ).appendChild( enviadoGIF );
        setTimeout(()=>{
            enviadoGIF.remove();
            frmEnviar.reset();
        },5000);
    }, 3000);

    e.preventDefault();
}

//Reset Form
function resetForm(e){
    resetForm.reset();
    inicioApp();
    e.preventDefault();
}

//Verifica la longitud del texto en los campos
function validarCampo() {
    //Se valida la longitud del texto y que no este vaio
    validarLongitud( this );
    //Validar unicamente email
    console.log(this.type);
    if (this.type === 'email') {
        validarEmail(this);
    }

    let errores = document.querySelectorAll( '.error' );
    if ( email.value !== '' && asunto.value !== '' && mensaje.value !== '') {
        if(errores.length === 0){
            btnEnviar.disabled = false; 
        }
    } else {
            btnEnviar.disabled = true;
    }
}

function validarLongitud(campo) {
    console.log("TCL: validarLongitud -> campo.value.length", campo.value.length);
    if( campo.value.length > 0 ){
        campo.style.borderBottomColor = 'green' ;
        campo.classList.remove( 'error' );
    } else {
        campo.style.borderBottomColor = 'red' ;
        campo.classList.add( 'error' );
    }
//console.log("TCL: validarLongitud -> campo", campo);

}

function validarEmail(email) {
    const mensaje = email.value;
    if (mensaje.indexOf('@') !== -1 ) {
        email.style.borderBottomColor = 'green' ;
        email.classList.remove( 'error' );
    } else {
        email.style.borderBottomColor = 'red' ;
        email.classList.add( 'error' );
    }
}