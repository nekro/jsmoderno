//  Variables
const todoList = document.getElementById('lista-tweets');

//  EventListeners
eventListeners();
function eventListeners(){
    //cuando se envie el formulario
    document.querySelector('#formulario').addEventListener('submit',addTodo);
    //borrar todos
    todoList.addEventListener('click',borrarTodo);
    //contenido cargado 
    document.addEventListener('DOMContentLoaded',readLocalStorage);
}

//  Functions

//AddTodo Form
function addTodo(e){
    e.preventDefault();
    //leer el valor del textarea
    const todo = document.getElementById('tweet').value;
    console.log("ToDo Value => " + todo);
    //Creando boton eliminar
    const botonBorrar = document.createElement('a');
    botonBorrar.classList = 'borrar-tweet';
    botonBorrar.innerText = 'X';

    //Crear Elemento y añadirlo a la lista
    const li = document.createElement('li');
    li.innerText = todo;
    //Añade el boton borrar
    li.appendChild(botonBorrar);
    //Agregamos el elemento con appendChild
    todoList.appendChild(li);
    //console.log(li);
    //Añadir a localStorage
    addLocalStorage(todo);
    
}

//Borrar Todo
function borrarTodo(e){
    e.preventDefault();
    if( e.target.className === 'borrar-tweet' ) {
        e.target.parentElement.remove();
        //console.log(e.target.parentElement);//Buscamos el padre del elemento a aliminar
       // console.log(e.target.parentElement.innerText);
        borrarTodosLocalStorage(e.target.parentElement.innerText);
    }
    //console.log('click en lista');
}

//Mostrar datos de localStorage
function readLocalStorage(){
    let lsTodos;
    lsTodos = obtenerTodosLocalStorage();
    
    lsTodos.forEach(function(todo){
        //Creando boton eliminar
    const botonBorrar = document.createElement('a');
    botonBorrar.classList = 'borrar-tweet';
    botonBorrar.innerText = 'X';

    //Crear Elemento y añadirlo a la lista
    const li = document.createElement('li');
    li.innerText = todo;
    //Añade el boton borrar
    li.appendChild(botonBorrar);
    //Agregamos el elemento con appendChild
    todoList.appendChild(li);
    });
}

//Agregar todo a localStorage
function addLocalStorage(todo){
    let todos;
    //leer los todos
    todos = obtenerTodosLocalStorage();
    //Añadir el nuevo ToDo
    todos.push(todo);
    //Convertir de String a arreglo de LS
    localStorage.setItem( 'todos', JSON.stringify(todos) );
    //Agregar a localstorage
    //localStorage.setItem('todoList', todo);
}

//Comprobar que existan elementos en localStorage retorna un arreglo
function obtenerTodosLocalStorage(){
    let todos;
    // Revisamos valores de local storage
    if(localStorage.getItem('todos') === null){
        todos = [];
    }else {
        todos = JSON.parse( localStorage.getItem('todos') );
    }
    return todos;
}

//Eliminar todos de localStorage
function borrarTodosLocalStorage(todo){
    let todos, deleteTodos;
    // Elimina la X del todo
    deleteTodos = todo.substring(0, todo.length - 1);
    todos = obtenerTodosLocalStorage();
    todos.forEach(function(todo,index){
        if( deleteTodos === todo ){
            todos.splice(index, 1);
            console.log()
        }
    });
    console.log( todos );
    localStorage.setItem('todos',JSON.stringify(todos));
}


