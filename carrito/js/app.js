//Variables
const carrito = document.getElementById( 'carrito' );
const cursos = document.getElementById( 'lista-cursos' );
const listaCursos = document.querySelector( '#lista-carrito tbody' );
const vaciarCarritoBtn = document.getElementById( 'vaciar-carrito' );
//Listeners 
cargarEventListeners();
function cargarEventListeners(){
    // Ejecucion cuando se dispara el evento => Agregar carrito
    cursos.addEventListener( 'click', comprarCurso );
    //Cuando se elimina un curso del carrito
    carrito.addEventListener( 'click', eliminarCurso );
    //Vaciar Carrito
    vaciarCarritoBtn.addEventListener( 'click', vaciarCarrito );
    // Al cargar el documento, leer los valores de localStorage
    document.addEventListener( 'DOMContentLoaded', leerLocalStorage);

}

//Functions
//Funcion que añade el curso al carrito
function comprarCurso(e){
    e.preventDefault();
    //console.log(e.target.classList);
    // Delegation para agregar a carrito
    if(e.target.classList.contains( 'agregar-carrito' ) ){
        //console.log('true');
        const curso = e.target.parentElement.parentElement;
        //console.log(curso);
        //Enviamos el curso seleccionado para enviar sus datos
        leerDatosCurso( curso );
    }
}

//Leer datos del curso
function leerDatosCurso( curso ){
    //Creamos un objeto para almacenar imagen titulo curso precio
    const infoCurso = {
        imagen : curso.querySelector( 'img' ).src,
        titulo : curso.querySelector( 'h4' ).textContent,
        precio : curso.querySelector( '.precio span' ).textContent,
        id: curso.querySelector( 'a' ).getAttribute( 'data-id' )
    }
    insertarCarrito(infoCurso);
    //console.log(curso);
    //console.log(infoCurso);
}

//Muestra el curso seleccionado en el carrito
function insertarCarrito(curso){
    //creamos un tr
    const row = document.createElement( 'tr' );
    //template a insertar dentro del tr
    row.innerHTML = `
        <td>
            <img src = "${curso.imagen}" width="100%"/>  
        </td>
        <td>
            <h4>"${curso.titulo}"</h4>
        </td>
        <td>
            "${curso.precio}"
        </td>
        <td>
            <a href = "#" class="borrar-curso" data-id="${curso.id}"> X </a>
        </td>
    `;
    //console.log(`value from function ${curso}`);
    listaCursos.appendChild( row );
    guardarCursoLocalStorage( curso );
}

//Elimina el curso del carrito en el DOM
function eliminarCurso(e){
    e.preventDefault();
    let curso,
        cursoId;
    if (e.target.classList.contains( 'borrar-curso' ) ) {
        e.target.parentElement.parentElement.remove();
        curso = e.target.parentElement.parentElement;
        cursoId = curso.querySelector( 'a' ).getAttribute( 'data-id' );
        //console.log("TCL: eliminarCurso -> cursoId", cursoId)
        
    } 
    //Eliminar cursos del LS
    eliminarCursoLocalStorage( cursoId );
}

//Elimina los cursos del carrito en el DOM
function vaciarCarrito(){
    //forma lenta
    //listaCursos.innerHTML = '';
    //forma rapida y recomendada con while verifica si existe un elemento con firstchilda
    while(listaCursos.firstChild){
        //mientras siga habiendo elementos los seguimos eliminando
        listaCursos.removeChild(listaCursos.firstChild);

    }
    //Vaciar Local Storage
    vaciarLocalStorage();
    return false;//para evitar el salto en la pantalla
}

//Almacena curso en localStorage desde el carrito
function guardarCursoLocalStorage(curso){
    let cursos;
    //toma el valor de un arreglo con datos de LS o vacio
    cursos = obtenerCursosLocalStorage();
    //console.log("TCL: guardarCursoLocalStorage -> cursos", cursos)
    //curso se agrega al arregloe
    cursos.push(curso);//curso se pasa entre las diferenets funciones y se mantiene el valor hasta aca :P
    localStorage.setItem( 'cursos', JSON.stringify(cursos) );
    //console.log("TCL: guardarCursoLocalStorage -> curso", curso)
}

//Comprueba que existan elementos en localStorage
function obtenerCursosLocalStorage(){
    let cursoLS;
    if( localStorage.getItem( 'cursos' ) === null ){
        cursoLS = [];
    } else {
        cursoLS = JSON.parse( localStorage.getItem( 'cursos' ) );
    }
    return cursoLS;
}

//Imprime los cursos de LS en el carrito
function leerLocalStorage() {
    let cursosLS;
    cursosLS = obtenerCursosLocalStorage();
    cursosLS.forEach( function(curso) {
        //construir el template
        const row = document.createElement( 'tr' );
        //template a insertar dentro del tr
        row.innerHTML = `
            <td>
                <img src = "${curso.imagen}" width="100%"/>  
            </td>
            <td>
                <h4>"${curso.titulo}"</h4>
            </td>
            <td>
                "${curso.precio}"
            </td>
            <td>
                <a href = "#" class="borrar-curso" data-id="${curso.id}"> X </a>
            </td>
        `;
        listaCursos.appendChild( row ); 
    });
    //console.log("TCL: leerLocalStorage -> cursosLS", cursosLS);
}

//Elimina curso por Id en localStorage
function eliminarCursoLocalStorage( curso ) {
    //console.log("TCL: eliminarCursoLocalStorage -> curso", curso)
    let cursosLS;
    //obtenemos el arreglo de cursos
    cursosLS = obtenerCursosLocalStorage();
    //iteramos comparando el id del curso con el de localStorage
    cursosLS.forEach( function( cursoLS, index ) {
    //console.log("TCL: eliminarCursoLocalStorage -> curso", curso)
        if ( cursoLS.id === curso ) {
            cursosLS.splice(index, 1);
        }
        
    });
    //console.log("TCL: eliminarCursoLocalStorage -> cursosLS", cursosLS)
    //console.log("TCL: eliminarCursoLocalStorage -> curso", curso)

    //Añadimos el arreglo actual a Local Storage
    localStorage.setItem( 'cursos', JSON.stringify(cursosLS) );
}

//Elimina los cursos de Local Storage
function vaciarLocalStorage() {
    localStorage.clear();
}