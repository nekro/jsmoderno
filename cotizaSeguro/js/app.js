function Seguro(marca, anio, tipo){
    this.marca = marca;
    this.anio = anio;
    this.tipo = tipo;
}

Seguro.prototype.cotizarSeguro = function(infoSeguro){
    /*  1 = Americano 1.15%
        2 = Asiatico 1.05%
        3 = Europeo 1.35%
    */
    let cantidad;
    const base = 2000;
    
    switch(this.marca){
        case '1':
            cantidad = base * 1.15;
            break;
        case '2':
            cantidad = base * 1.05;
            break;
        case '3':
            cantidad = base * 1.35;
            break;
    }
    
   // console.log("TCL: Seguro.prototype.cotizarSeguro -> cantidad", cantidad);
    //  Leer año
    const anioDiferencia = new Date().getFullYear() - this.anio;
   // console.log("TCL: Seguro.prototype.cotizarSeguro -> anioDiferencia", anioDiferencia);    
    // Cada año de diferencia hay que reducir 3% el valor del seguro que es cantidad
    cantidad -= ((anioDiferencia *3)* cantidad) / 100;
   // console.log("TCL: Seguro.prototype.cotizarSeguro -> cantidad restada", cantidad)
    /*
        Si el seguro es basico se multiplica por 30% mas
        Si el seguro es completo 50% mas
    */
    if(this.tipo === 'basico'){
        cantidad *= 1.30;
    } else {
        cantidad *= 1.50;
    }
    return cantidad;
    //console.log("TCL: Seguro.prototype.cotizarSeguro -> cantidad", cantidad)
    //gracias a los prototypes podemos acceder a estos valores
    /* console.log("Marca",this.marca);
    console.log("Marca",this.anio);
    console.log("Marca",this.tipo);
    console.log("TCL: infoSeguro", infoSeguro); */
}
//Lo que se muestra en la interfaz
function Interfaz(){}

//mensaje que impime en html mostrarError
Interfaz.prototype.mostrarError = function(mensaje, tipoError){
    //Creamos div par amostrar mensaje    
    const div = document.createElement('div');
    if(tipoError === 'error'){
        div.classList.add('mensaje', 'error');
    } else {
        div.classList.add('mensaje','correcto');
    }
    div.innerHTML = `${mensaje}`;
    formulario.insertBefore(div, document.querySelector('.form-group'));

    //Desaparecer Mensaje
    setTimeout(function(){
        document.querySelector('.mensaje').remove();
    }, 3000);
}


//Event Listeners
const formulario = document.getElementById('cotizar-seguro');
formulario.addEventListener( 'submit', function(e){
    e.preventDefault();

    //Leer la marca seleccionada en el Select
    const marca = document.getElementById( 'marca' );
    const marcaSeleccionada = marca.options[marca.selectedIndex].value;
    
    //console.log("TCL: marca", marca)
    //console.log("TCL: marcaSeleccionada", marcaSeleccionada)

    //Leer el año del select años :P
    const anio = document.getElementById( 'anio' );
    const anioSeleccionado = anio.options[anio.selectedIndex].value;
    //console.log("TCL: anio", anio)
    //console.log("TCL: anioSeleccionado", anioSeleccionado)
    
    //Lee le valor de los radio button
    const tipo = document.querySelector('input[name="tipo"]:checked').value;
    
    //Crear instancia de interfaz
    const interfaz = new Interfaz();

    //Revisamos que los campos no esten vacios
    if(marcaSeleccionada === "" || anioSeleccionado === "" || tipo === ""){
        //Interfaz Imprimiendo un error
        interfaz.mostrarError('Faltan Datos revisar el formulario y envia nuevamente', 'error');
    } else {
        //instanciar seguro y mostrar interfaz
        const seguro = new Seguro(marcaSeleccionada,anioSeleccionado,tipo);
        //verificamos que se manden los datos correctos
        //console.log("TCL: seguro", seguro)
        //Cotizar el seguro con un prototype que solo pertenece a seguro y devuelve una cantidad
        const cantidad = seguro.cotizarSeguro();
        console.log("TCL: cantidad", cantidad)
        
    }
});


const max = new Date().getFullYear();
const min = max -20;
//console.log("min "+ min);
//console.log("max "+ max);


const selectYears =  document.getElementById( 'anio' );
for(let i = max; i > min; i--){
    let option = document.createElement( 'option' );
    option.value = i;
    option.innerHTML = i;
    selectYears.appendChild( option );
}